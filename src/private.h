#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <pthread.h>

#include "AL/al.h"

#include "math.h"

#define MAX_CONTEXTS 1

/* This is the max number of sources that can be generated, but
 * the max number that can be played concurrently is at most 64
 * (if all mono) */
#define MAX_SOURCES_PER_CONTEXT 256
#define MAX_BUFFERS_PER_CONTEXT 1024

ALboolean _alSetError(const char* func, ALenum error, const ALchar* msg);
bool _alIsValidEnum(const ALenum* valid, ALenum test);

typedef struct {
    Vec3 position;
    Vec3 velocity;
} Transform;

void _alInitTransform(Transform* transform);

typedef struct Buffer {
    ALuint name;
    bool is_dead;

    ALenum format;
    ALsizei data_size;
    ALsizei alloc_size; // Possibly more due to rounding requirements
    ALsizei sample_count;

    /* The DC SPU works with address offsets from some base address
     * rather than pointers. That's what data is */
    intptr_t data;
    ALsizei freq;
} Buffer;

void _alInitBuffer(Buffer* buffer);

struct _BufferQueueEntry;

typedef struct _BufferQueueEntry {
    Buffer* buffer;
} BufferQueueEntry;

typedef struct _BufferQueueIterator {
    /* When we start playing a buffer, we set the timestamp
     * that the buffer should complete, then use that to change
     * the state. */

    ALint queue_index;

    /* Mono sources will only use one channel, if channels[0] == -1
     * then no channels have been allocated */
    int channels[2];

    uint32_t last_sample;
    uint64_t end_time;

    /* We must store these values so that when
     * starting a new buffer we maintain what had
     * previously been set for the source that's playing */
    float pan;
    float gain;

} BufferQueueIterator;


#define MAX_BUFFERS_PER_QUEUE 8


typedef struct {
    Transform transform;

    ALuint name;
    bool is_dead;

    float reference_distance;
    float rolloff_factor;
    float max_distance;
    float gain;
    float pitch;

    ALboolean looping;

    ALenum state;
    ALenum type;
    ALboolean relative;

    ALint buffers_processed;

    BufferQueueEntry buffer_queue[MAX_BUFFERS_PER_QUEUE];
    ALint buffer_queue_front;
    ALint buffer_queue_back;

    BufferQueueIterator buffer_queue_iterator;

    pthread_mutex_t mutex;
} Source;

static inline uint16_t source_queue_size(const Source* source) {
    if(source->buffer_queue_front <= source->buffer_queue_back) {
        return source->buffer_queue_back - source->buffer_queue_front;
    } else {
        return (source->buffer_queue_back + MAX_BUFFERS_PER_QUEUE) - source->buffer_queue_front;
    }
}

static inline uint16_t source_queue_available(const Source* source) {
    return MAX_BUFFERS_PER_QUEUE - source_queue_size(source);
}

static inline bool source_queue_empty(const Source* source) {
    return source_queue_available(source) == MAX_BUFFERS_PER_QUEUE;
}

static inline bool source_queue_push(Source* source, const BufferQueueEntry* entry) {
    assert(source_queue_available(source));
    if(!source_queue_available(source)) {
        return false;
    }

    uint16_t new_idx = (source->buffer_queue_back % MAX_BUFFERS_PER_QUEUE);
    source->buffer_queue[new_idx] = *entry;
    source->buffer_queue_back = (source->buffer_queue_back + 1) % MAX_BUFFERS_PER_QUEUE;

    assert(source->buffer_queue_back <= MAX_BUFFERS_PER_QUEUE);
    assert(source->buffer_queue_front <= MAX_BUFFERS_PER_QUEUE);

    return true;
}

static inline bool source_queue_pop(Source* source) {
    assert(!source_queue_empty(source));
    if(source_queue_empty(source)) {
        return false;
    }

    memset(&source->buffer_queue[source->buffer_queue_front], 0, sizeof(BufferQueueEntry));
    source->buffer_queue_front = (source->buffer_queue_front + 1) % MAX_BUFFERS_PER_QUEUE;

    assert(source->buffer_queue_back <= MAX_BUFFERS_PER_QUEUE);
    assert(source->buffer_queue_front <= MAX_BUFFERS_PER_QUEUE);

    return true;
}

static inline BufferQueueEntry* source_queue_front(Source* source) {
    if(source_queue_size(source) == 0) {
        return NULL;
    }

    assert(source->buffer_queue_back <= MAX_BUFFERS_PER_QUEUE);
    assert(source->buffer_queue_front <= MAX_BUFFERS_PER_QUEUE);

    return &source->buffer_queue[source->buffer_queue_front];
}

static inline BufferQueueEntry* source_queue_at(Source* source, int16_t i) {
    assert(i != -1);

    if(i == -1) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Tried to access a finished iterator?");
        return NULL;
    }

    if(source_queue_empty(source)) {
        return NULL;
    }

    assert(i < MAX_BUFFERS_PER_QUEUE);

    uint16_t idx = (source->buffer_queue_front + i) % MAX_BUFFERS_PER_QUEUE;
    assert(idx <= MAX_BUFFERS_PER_QUEUE);
    return &source->buffer_queue[idx];
}


void _alInitSource(Source* source);

typedef struct {
    Transform transform;

    Vec3 right;
    Vec3 up;
    Vec3 forward;
} Listener;

void _alInitListener(Listener* listener);

struct _ALCdevice_struct;

typedef struct _ALCcontext_struct {
    struct _ALCdevice_struct* device;

    Buffer null_buffer;
    Buffer buffers[MAX_BUFFERS_PER_CONTEXT];
    Source sources[MAX_SOURCES_PER_CONTEXT];

    int buffer_count;
    int source_count;

    bool is_dead;

    pthread_mutex_t mutex;
} ALCcontext_struct;

typedef struct _ALCdevice_struct {
    const char* name;
    struct _ALCcontext_struct contexts[MAX_CONTEXTS];

    bool is_open;

    struct _ALCdevice_struct* next;
} ALCdevice_struct;

typedef ALCdevice_struct Device;
typedef ALCcontext_struct Context;

void _alResetContext(Context* context);
void _alInitDevice(Device* device, const char* name);
void _alIteratorReleaseChannels(BufferQueueIterator* iterator);
void _alSourceUnqueueBuffersUnlocked(Source* obj, ALsizei nb, ALuint *buffers);

Listener* _alListener();
Buffer* _alBuffer(ALuint buffer);
Context* _alContext();
Source* _alSource(ALuint source);
Source* _alSourceUnlocked(ALuint source);
void _alSourceStopUnlocked(Source* source);
ALenum _alDistanceModel();


/* RAII locking */

typedef struct {
    pthread_mutex_t* mutex;
} ScopedLock ;

inline static void unlock(ScopedLock* mutex) {
    pthread_mutex_unlock(mutex->mutex);
}

#define SCOPED_LOCK(mutex) \
    assert(mutex); \
    ScopedLock _lock __attribute__((cleanup(unlock))) = {mutex}; \
    (void) _lock; \
    pthread_mutex_lock(mutex);
