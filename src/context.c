#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#ifdef __DREAMCAST__
#include <kos.h>
#else
#include <pthread.h>
#endif

#include "private.h"
#include "platform.h"
#include "math.h"

#include "../include/AL/alc.h"

#define _THREAD_DEBUG 0

#ifdef __DREAMCAST__
typedef kthread_t ThreadType;

ThreadType* ThreadCreate(
    int detach,
    void *(*routine)(void *param),
	void *param
) {
    return thd_create(detach, routine, param);
}

void ThreadSetPriority(ThreadType* thread, int priority) {
    thd_set_prio(thread, priority);
}

void ThreadJoin(ThreadType* thread, void* ret) {
    thd_join(thread, ret);
}

void ThreadPass() {
    thd_sleep(10);
}

#else
typedef pthread_t ThreadType;

#define PRIO_DEFAULT 10

ThreadType* ThreadCreate(
    int detach,
    void *(*routine)(void *param),
	void *param
) {
    static ThreadType thread;
    pthread_create(&thread, 0, routine, param);
    return &thread;
}

void ThreadSetPriority(ThreadType* thread, int priority) {
    // no-op
}

void ThreadJoin(ThreadType* thread, void* ret) {
    pthread_join(*thread, ret);
}

void ThreadPass() {
    usleep(0);
}

#endif


static inline __attribute__((always_inline)) float fsrra(float x) {
#ifdef _arch_dreamcast
    asm volatile ("fsrra %[one_div_sqrt]\n"
    : [one_div_sqrt] "+f" (x) // outputs, "+" means r/w
    : // no inputs
    : // no clobbers
    );

    return x;
#else
    return 1.0f / sqrtf(x);
#endif
}

static inline __attribute__((always_inline)) float fast_divide(float d, float n) {
    const float sgn = (n > 0) - (n < 0);
    return sgn * fsrra(n * n) * d;
}


static Context* CURRENT = NULL;
static ThreadType* DEVICE_THREAD = NULL;

static Device DEVICES [] = {
    {"Yamaha ARM7 (67 Mhz) Analog Output", {}, false, NULL}
};

static const uint32_t NUM_DEVICES = sizeof(DEVICES) / sizeof(Device);

static bool INITIALIZED = false;

static void _alInitDevices() {
    if(INITIALIZED) {
        return;
    }

    for(int i = 0; i < NUM_DEVICES; ++i) {
        DEVICES[i].is_open = false;
    }

    init_spu();
    _alInitListener(_alListener());


    INITIALIZED = true;
}

static char DEVICE_STRING[1024];

static float _alCalculatePanning(const Source* s) {
    static const float half_pi = M_PI * 0.5f;
    static const float inv_half_pi = 1.0f / half_pi;
    static const float E = 0.000001f;

    /* Given a source, returns a value between -1.0f and 1.0f */
    Listener* l = _alListener();
    Vec3 N = l->forward;

    assert(l);

    if(!l) {
        return 0.0f;
    }

    /* Handle zero direction */
    if(abs(N.x) < E && abs(N.y) < E && abs(N.z) < E) {
        return 0.0f;  /* Return no panning (center) */
    }

    float NL = _alVec3Length(&l->forward);

    Vec3 U, V;
    _alVec3Cross(&U, &N, &l->up);
    _alVec3Normalize(&U);

    _alVec3Cross(&V, &N, &U);
    _alVec3Normalize(&V);

    _alVec3Normalize(&N);

    Vec3 position = s->transform.position;
    if(s->relative) {
        _alVec3Subtract(&position, &position, &l->transform.position);
    }

    Vec3 rotated = {
        _alVec3Dot(&position, &U),
        -_alVec3Dot(&position, &V),
        -_alVec3Dot(&position, &N),
    };

    float mags = NL * _alVec3Length(&rotated);
    float radians = (mags == 0.0f) ? 0.0f : acosf(_alVec3Dot(&l->forward, &rotated) / mags);

    if(fabs(radians) > half_pi) {
        radians = M_PI - radians;
    }

    if (rotated.x < 0.0f) {
        radians = -radians;
    }

    /* This would be -1 if the sound was directly left, +1 if directly
     * right. 0 if front or back */
    return radians * inv_half_pi;
}

static float _alCalculateGain(Source* s, const Vec3* dir) {
    ALenum distance_model = _alDistanceModel();
    float gain = 1.0f;
    float distance = _alVec3Length(dir);
    float ref_distance = s->reference_distance;
    float roll_off = s->rolloff_factor;

#define CLAMP_DISTANCE() \
    distance = (distance > ref_distance) ? distance : ref_distance; \
    distance = (distance < s->max_distance) ? distance : s->max_distance

    switch(distance_model) {
        case AL_INVERSE_DISTANCE_CLAMPED:
            CLAMP_DISTANCE();  /* Explicit fallthrough */
        case AL_INVERSE_DISTANCE:
            gain = fast_divide(ref_distance, (__builtin_fmaf(roll_off, (distance - ref_distance), ref_distance)));
        break;
        case AL_LINEAR_DISTANCE_CLAMPED:
            CLAMP_DISTANCE(); /* Explicit fallthrough */
        case AL_LINEAR_DISTANCE:
            // FIXME: Implement
        break;
        case AL_EXPONENT_DISTANCE_CLAMPED:
            CLAMP_DISTANCE();  /* Explicit fallthrough */
        case AL_EXPONENT_DISTANCE:
            // FIXME: Implement
        break;
        case AL_NONE:
        default:
             break;
    }

    return gain;
}

static void _alPlayEntry(Source* source, BufferQueueIterator* iterator) {

    BufferQueueEntry* entry = source_queue_at(source, iterator->queue_index);
    if(!entry) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Tried to play entry on empty source queue");
        return;
    }

    Buffer* buffer = entry->buffer;
    assert(buffer);

    if(!buffer) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Iterator without a buffer?!");
        return;
    }

    ALenum format = buffer->format;

    uint8_t bits = (
        buffer->format == AL_FORMAT_MONO16 ||
        buffer->format == AL_FORMAT_STEREO16
    ) ? 16 : 8;

    uint8_t channels = (
        format == AL_FORMAT_STEREO8 ||
        format == AL_FORMAT_STEREO16
    ) ? 2 : 1;

    iterator->last_sample = 0;
    iterator->channels[0] = (iterator->channels[0] == -1) ? alloc_spu_channel() : iterator->channels[0];
    if(iterator->channels[0] == -1) {
        printf("[ALdc] Unable to allocate channel, all are being used");
        source->state = AL_STOPPED;
        return;
    }

    uint64_t now = time_in_ms();

    if(channels == 2) {
        /* Allocate channels if necessary */
        iterator->channels[1] = (iterator->channels[1] == -1) ? alloc_spu_channel() : iterator->channels[1];

        if(iterator->channels[1] == -1) {
            printf("[ALdc] Unable to allocate channel, all are being used");
            source->state = AL_STOPPED;
            return;
        }

        assert(iterator->channels[0] > -1);
        assert(iterator->channels[1] > -1);

        play_channels(
            iterator->channels[0],
            iterator->channels[1],
            buffer->data,
            buffer->data + (buffer->alloc_size / 2),
            buffer->freq,
            bits,
            (buffer->sample_count / 2),
            iterator->gain
        );
    } else {
        assert(iterator->channels[0] > -1);

        /* If this assertion fires then we had a sound that didn't release
         * its channel properly */
        assert(iterator->channels[1] == -1);
        iterator->channels[1] = -1;

        play_channel(
            iterator->channels[0],
            buffer->data,
            buffer->freq,
            bits,
            buffer->sample_count,
            iterator->gain,
            iterator->pan
        );
#if _THREAD_DEBUG
        printf(
            "Playing channel: %d (%d)\n",
            iterator->channels[0], buffer->sample_count
        );
#endif
    }

    float samples_per_channel = buffer->sample_count / channels;

    uint64_t duration = (samples_per_channel / ((float)buffer->freq)) * 1000.0f;
    iterator->end_time = now + duration;
}

static bool _updateSamplePos(BufferQueueIterator* iterator) {
    /*
     * Update the last_sample on the iterator and return true
     * if we just hit the end */

    /* If this platform supports checking to see if a channel
    is finished, we use that, otherwise just go by time passed */

    if(use_time_based_buffer_switching()) {
        uint64_t time = time_in_ms();
        bool finished = time >= iterator->end_time;
        return finished;
    } else {
        return channel_done(iterator->channels[0]);
    }
}

const static Vec3 ZERO = {0, 0, 0};

void _alIteratorReleaseChannels(BufferQueueIterator* iterator) {
    for(int i = 0; i < 2; ++i) {
        if(iterator->channels[i] > -1) {
            stop_channel(iterator->channels[i]);
            release_spu_channel(iterator->channels[i]);
            iterator->channels[i] = -1;
        }
    }
}

static void _alUpdateSource(Source* s) {
    /* If the source is dead, then don't do anything else */
    if(s->is_dead) {
        return;
    }

    /* We lock the source, because this is a background thread
    and the main thread could be queuing stuff etc. It would be nice
    if we could make this lock-less eventually */
    SCOPED_LOCK(&s->mutex);

    BufferQueueIterator* iterator = &s->buffer_queue_iterator;
    BufferQueueEntry* entry = iterator->queue_index > -1 ? source_queue_at(s, iterator->queue_index) : NULL;
    Buffer* b = (entry) ? entry->buffer : NULL;

    /* The source was set to playing somehow, but there are no allocated channels
    so we play using the iterator which will do that allocation */

    if(s->state == AL_PLAYING) {
        if(iterator->channels[0] == -1) {
            /* No channels allocated, but we're playing,
                * so let's begin */
            _alPlayEntry(s, iterator);
        }
    }

    bool buffer_finished = false;

    if(s->state == AL_PLAYING) {
        buffer_finished = _updateSamplePos(iterator);
        if(!buffer_finished) {
            assert(b);
            bool is_mono = (
                b->format == AL_FORMAT_MONO8 ||
                b->format == AL_FORMAT_MONO16
            );

            if(s->state == AL_PLAYING && is_mono) {
                /* Still playing? Let's do some positional things! Although
                    * only if it's a mono buffer that's playing */

                Vec3 dir;
                Listener* l = _alListener();

                /* If the position is relative, then the direction is just the negative
                vector, otherwise it's the direction from the listener to the source */
                const Vec3* origin = (s->relative) ? &ZERO : &l->transform.position;
                _alVec3Subtract(&s->transform.position, origin, &dir);

                iterator->pan = _alCalculatePanning(s);
                if(iterator->channels[0] > -1) {
                    pan_channel(iterator->channels[0], iterator->pan);
                }

                iterator->gain = _alCalculateGain(s, &dir);
                if(iterator->channels[0] > -1) {
                    amp_channel(iterator->channels[0], iterator->gain);
                }
            }

            if(s->state == AL_PLAYING) {
                pitch_channel(
                    iterator->channels[0],
                    (int) (((float) b->freq) * s->pitch)
                );
            }
        }
    } else if(s->state == AL_STOPPED && iterator->channels[0] > -1) {
        buffer_finished = true;
    }

    if(buffer_finished) {
        /* We've reached the end of the buffer! */
        if(s->state == AL_PLAYING) {
            /* If we're playing, move to the next buffer */
            assert(iterator->queue_index > -1);
            iterator->queue_index++;

            if(iterator->queue_index == source_queue_size(s)) {
                iterator->queue_index = -1;
            }

            BufferQueueEntry* entry = (iterator->queue_index > -1) ? source_queue_at(s, iterator->queue_index) : NULL;
            /* Mark the buffers as processed only if we're not
             * looping */
            if(!s->looping) {
                s->buffers_processed++;
            }

            /* We reached the end, so mark as stopped, or loop */
            if(!entry) {
                _alIteratorReleaseChannels(iterator);

                if(s->looping) {
                    s->state = AL_PLAYING;
                    s->buffers_processed = 0;
                    iterator->queue_index = source_queue_empty(s) ? -1 : 0;
                    //assert(s->buffer_queue_iterator.entry);
                    _alPlayEntry(s, iterator);
                } else {
                    s->state = AL_STOPPED;
                }
            } else {
                /* Make sure we queue the next buffer */
                _alPlayEntry(s, iterator);
            }
        } else {
            _alIteratorReleaseChannels(iterator);
        }
    }
}

static void* _alDeviceThread(void* dev) {
    /* Where the magic happens */
    Device* device = (Device*) dev;

    printf("OpenAL: Starting device thread\n");

    while(device->is_open) {
        for(int i = 0; i < MAX_CONTEXTS; ++i) {
            Context* ctx = &device->contexts[i];

            /* Dead contexts don't need updating */
            if(ctx->is_dead) {
                continue;
            }

            {
                /* Loop through and update each source */
                for(int j = 0; j < MAX_SOURCES_PER_CONTEXT; ++j) {
                    SCOPED_LOCK(&ctx->mutex);
                    Source* s = &ctx->sources[j];
                    if(!s->is_dead) {
                        _alUpdateSource(s);
                        ThreadPass();
                    }
                }

                usleep(5000);
            }
        }
    }

    printf("OpenAL: Stopping device thread\n");
    return NULL;
}

void _alResetContext(Context* context) {
    _alInitBuffer(&context->null_buffer);

    pthread_mutex_init(&context->mutex, NULL);

    context->device = NULL;
    context->is_dead = true;
    context->buffer_count = 0;
    context->source_count = 0;

    for(int i = 0; i < MAX_SOURCES_PER_CONTEXT; ++i) {
        context->sources[i].is_dead = true;
    }

    for(int i = 0; i < MAX_BUFFERS_PER_CONTEXT; ++i) {
        _alInitBuffer(&context->buffers[i]);
        context->buffers[i].is_dead = true;
    }
}

void _alInitDevice(Device* device, const char* name) {
    device->name = name;
    device->is_open = false;
}

AL_API ALCcontext* AL_APIENTRY alcCreateContext(ALCdevice *device, const ALCint* attrlist) {
    if(attrlist) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    }

    Context* ret = NULL;
    Device* dev = (Device*) device;

    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        if(dev->contexts[i].is_dead) {
            ret = &dev->contexts[i];
            ret->is_dead = false;
            break;
        }
    }

    if(!ret) {
        _alSetError(__func__, ALC_INVALID_VALUE, "Out of contexts");
    }

    return (ALCcontext*) ret;
}

AL_API ALCboolean AL_APIENTRY alcMakeContextCurrent(ALCcontext *context) {
    CURRENT = (Context*) context;
    return AL_TRUE;
}

AL_API void AL_APIENTRY alcProcessContext(ALCcontext *context) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcSuspendContext(ALCcontext *context) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcDestroyContext(ALCcontext *context) {
    if(context == (ALCcontext*) CURRENT) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Tried to delete the current context");
        return;
    }

    Context* c = (Context*) context;
    pthread_mutex_destroy(&c->mutex);
    _alResetContext(c);  /* Reset */
}

AL_API ALCcontext* AL_APIENTRY alcGetCurrentContext(void) {
    return (ALCcontext*) CURRENT;
}

AL_API ALCdevice* AL_APIENTRY alcGetContextsDevice(ALCcontext *context) {
    if(!context) {
        _alSetError(__func__, AL_INVALID_OPERATION, "Context was NULL");
        return NULL;
    }

    return (ALCdevice*) ((Context*) context)->device;
}

AL_API ALCdevice* AL_APIENTRY alcOpenDevice(const ALCchar *devicename) {
    _alInitDevices();

    Device* to_open = NULL;

    if(devicename == NULL || strlen(devicename) == 0) {
        to_open = &DEVICES[0];
    } else {
        for(int i = 0; i < NUM_DEVICES; ++i) {
            Device* dev = &DEVICES[i];
            if(strcmp(dev->name, devicename) == 0) {
                to_open = dev;
                break;
            }
        }
    }

    assert(to_open);

    if(to_open->is_open) {
        fprintf(stderr, "Already opened device (%s)???\n", to_open->name);
        return NULL;
    }

    to_open->is_open = true;

    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        _alResetContext(&to_open->contexts[i]);
    }

    DEVICE_THREAD = ThreadCreate(0, &_alDeviceThread, (void*) to_open);

    return (ALCdevice*) to_open;
}

AL_API ALCboolean AL_APIENTRY alcCloseDevice(ALCdevice *device) {
    Device* dev = (Device*) device;

    /* Make sure all buffers were destroyed */
    for(int i = 0; i < MAX_CONTEXTS; ++i) {
        Context* ctx = &dev->contexts[i];
        if(!ctx->is_dead) {
            fprintf(stderr, "Couldn't close device with contexts\n");
            return AL_FALSE;
        }

        if(ctx->buffer_count) {
            return AL_FALSE;
        }
    }

    dev->is_open = false;

    void* ret;
    if(DEVICE_THREAD) {
        ThreadJoin(DEVICE_THREAD, &ret);
    } else {
        printf("Device thread already finished?\n");
    }

    return AL_TRUE;
}

AL_API ALCenum AL_APIENTRY alcGetError(ALCdevice *device) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return (ALCenum) 0;
}

AL_API ALCboolean AL_APIENTRY alcIsExtensionPresent(ALCdevice *device, const ALCchar *extname) {
    if(strcmp(extname, "ALC_ENUMERATION_EXT") == 0) {
        return AL_TRUE;
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API void* AL_APIENTRY alcGetProcAddress(ALCdevice *device, const ALCchar *funcname) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return NULL;
}

AL_API ALCenum AL_APIENTRY alcGetEnumValue(ALCdevice *device, const ALCchar *enumname) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return (ALCenum) 0;
}

AL_API const ALCchar* AL_APIENTRY alcGetString(ALCdevice *device, ALCenum param) {
    const ALenum VALID [] = {
        ALC_EXTENSIONS,
        ALC_DEVICE_SPECIFIER,
        ALC_DEFAULT_DEVICE_SPECIFIER,
        0
    };

    if(!_alIsValidEnum(VALID, param)) {
        _alSetError(__func__, AL_INVALID_ENUM, "Invalid enum");
        return "";
    }

    if(param == ALC_DEVICE_SPECIFIER) {
        _alInitDevices();
        sprintf(DEVICE_STRING, "%s%c%c", DEVICES->name, '\0', '\0');
        return DEVICE_STRING;
    } else if(param == ALC_DEFAULT_DEVICE_SPECIFIER) {
        _alInitDevices();
        return DEVICES[0].name;
    } else {
        assert(0 && "Invalid param?");
    }

    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return "";
}

AL_API void AL_APIENTRY alcGetIntegerv(ALCdevice *device, ALCenum param, ALCsizei size, ALCint *values) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API ALCdevice* AL_APIENTRY alcCaptureOpenDevice(const ALCchar *devicename, ALCuint frequency, ALCenum format, ALCsizei buffersize) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return NULL;
}

AL_API ALCboolean AL_APIENTRY alcCaptureCloseDevice(ALCdevice *device) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
    return AL_FALSE;
}

AL_API void AL_APIENTRY alcCaptureStart(ALCdevice *device) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcCaptureStop(ALCdevice *device) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

AL_API void AL_APIENTRY alcCaptureSamples(ALCdevice *device, ALCvoid *buffer, ALCsizei samples) {
    _alSetError(__func__, AL_INVALID_OPERATION, "Not Implemented");
}

Context* _alContext() {
    return CURRENT;
}
