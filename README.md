# ALdc

ALdc is an OpenAL implementation for the SEGA Dreamcast

ALdc is a ground-up implementation of the OpenAL 1.1 API for the SEGA Dreamcast - designed to leverage as much as possible of the Dreamcast AICA SPU.


# Building

```
mkdir builddir
cd builddir
cmake -DCMAKE_TOOLCHAIN_FILE=../toolchains/Dreamcast.cmake -DCMAKE_BUILD_TYPE=Release ..
make
```
