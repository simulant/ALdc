cmake_minimum_required(VERSION 3.17)
project(aldc2)

set(sources
  src/buffer.c
  src/core.c
  src/listener.c
  src/source.c
  src/alut.c
  src/context.c
  src/platform.c
  src/math.c
)

if(NOT PLATFORM_DREAMCAST)
find_package(Threads)
find_package(SDL2 REQUIRED)
find_package(OpenGL REQUIRED)
link_libraries(${SDL2_LIBRARIES} ${OPENGL_LIBRARIES} m)
endif()

set(CMAKE_C_STANDARD 99)

include_directories(${CMAKE_SOURCE_DIR}/include)

link_libraries(${CMAKE_THREAD_LIBS_INIT})
add_compile_options(-Wall)
add_library(ALdc ${sources})

function(gen_sample sample)
    set(SAMPLE_SRCS ${ARGN})
    set(GENROMFS "$ENV{KOS_BASE}/utils/genromfs/genromfs")
    set(BIN2O $ENV{KOS_BASE}/utils/bin2o/bin2o)
    set(ROMDISK_IMG "${CMAKE_SOURCE_DIR}/samples/${sample}/romdisk.img")
    set(ROMDISK_O "${CMAKE_SOURCE_DIR}/samples/${sample}/romdisk.o")
    set(ROMDISK_DIR "${CMAKE_SOURCE_DIR}/samples/${sample}/romdisk")

    add_executable(${sample} ${SAMPLE_SRCS})
    target_link_libraries(${sample} ALdc)

    if(PLATFORM_DREAMCAST)
        if(EXISTS "${CMAKE_SOURCE_DIR}/samples/${sample}/romdisk")
            message("Generating romdisk for sample: ${sample}")
            add_custom_command(
                OUTPUT ${ROMDISK_IMG}
                COMMAND ${GENROMFS} -f ${ROMDISK_IMG} -d ${ROMDISK_DIR} -v
            )

            add_custom_command(
                OUTPUT ${ROMDISK_O}
                COMMAND ${BIN2O} romdisk.img romdisk romdisk.o
                DEPENDS ${ROMDISK_IMG}
                WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/samples/${sample}"
            )

            add_custom_target(${sample}_romdisk DEPENDS ${ROMDISK_O})
            add_dependencies(${sample} ${sample}_romdisk)

            target_include_directories(${sample} PUBLIC ${CMAKE_SOURCE_DIR}/GLdc/include)
            target_link_directories(${sample} PUBLIC ${CMAKE_SOURCE_DIR}/GLdc)
            target_link_libraries(${sample} ${ROMDISK_O} ${CMAKE_SOURCE_DIR}/GLdc/libGLdc.a ALdc)
        else()
            message("No such romdisk for sample: ${sample} at 'samples/${sample}/romdisk'")
        endif()
    endif()
endfunction()

gen_sample(basic samples/basic/main.c)
gen_sample(helicopter samples/helicopter/main.c)
gen_sample(streaming samples/streaming/main.c)
gen_sample(weird_sounds samples/weird_sounds/main.c samples/weird_sounds/audio_assist.c)
